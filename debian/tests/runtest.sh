#!/bin/sh

set -e

for file in /usr/bin/minikerberos-*; do
    $file -h
done
